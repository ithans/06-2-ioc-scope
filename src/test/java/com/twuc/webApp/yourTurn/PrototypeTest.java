package com.twuc.webApp.yourTurn;

import com.twuc.webApp.prototype.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

public class PrototypeTest {
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.prototype");

    @Test
    void test_the_same_class() {
        SimplePrototypeScopeClass bean = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass bean1 = context.getBean(SimplePrototypeScopeClass.class);
        assertNotSame(bean, bean1);
    }

    @Test
    void test_init() {
        SimplePrototypeScopeClass simplePrototypeScopeClass = context.getBean(SimplePrototypeScopeClass.class);
        SimplePrototypeScopeClass simplePrototypeScopeClass1 = context.getBean(SimplePrototypeScopeClass.class);
        ChildObject childObject = context.getBean(ChildObject.class);
        ChildObject childObject1 = context.getBean(ChildObject.class);
        MyLogger myLogger = context.getBean(MyLogger.class);
        Assertions.assertIterableEquals(Arrays.asList("childInit", "simplePrototype", "simplePrototype"), myLogger.getLines());
    }

    @Test
    void lazy_init() {
        MyLogger myLogger = context.getBean(MyLogger.class);
        Assertions.assertEquals(0, myLogger.getLines().size());
        SimpleSingletonInitTime initTime = context.getBean(SimpleSingletonInitTime.class);
        Assertions.assertIterableEquals(Arrays.asList("SimpleSingletonInitTime init"), myLogger.getLines());
    }

    @Test
    void test_singleton_init_count() {
        MyLogger myLogger = context.getBean(MyLogger.class);
        PrototypeScopeDependsOnSingleton prototypeScopeDependsOnSingleton = context.getBean(PrototypeScopeDependsOnSingleton.class);
        PrototypeScopeDependsOnSingleton prototypeScopeDependsOnSingleton2 = context.getBean(PrototypeScopeDependsOnSingleton.class);
        Assertions.assertIterableEquals(Arrays.asList("init SingletonDependent"), myLogger.getLines());

    }

}
