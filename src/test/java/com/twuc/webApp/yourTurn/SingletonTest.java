package com.twuc.webApp.yourTurn;

import com.twuc.webApp.singleton.*;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import static org.junit.jupiter.api.Assertions.assertNotSame;
import static org.junit.jupiter.api.Assertions.assertSame;

public class SingletonTest {
    AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext("com.twuc.webApp.singleton");

    @Test
    void test_interface_same() {
        InterfaceOne bean = context.getBean(InterfaceOneImpl.class);
        InterfaceOne bean1 = context.getBean(InterfaceOne.class);
        assertSame(bean,bean1);
    }

    @Test
    void test_class_same() {
        Extend bean = context.getBean(InterfaceOneImpl.class);
        Extend bean1 = context.getBean(Extend.class);
        assertSame(bean,bean1);
    }

    @Test
    void test_abstract_same() {
        AbstractBaseClass bean = context.getBean(AbstractBaseClass.class);
        DerivedClass bean1 = context.getBean(DerivedClass.class);
        assertSame(bean,bean1);
    }
}
