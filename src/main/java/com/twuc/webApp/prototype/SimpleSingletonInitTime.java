package com.twuc.webApp.prototype;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope()
@Lazy
public class SimpleSingletonInitTime {
    private MyLogger logger;

    public SimpleSingletonInitTime(MyLogger logger) {
        this.logger = logger;
        logger.log("SimpleSingletonInitTime init");
    }
}
