package com.twuc.webApp.prototype;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependent {
    private MyLogger myLogger;

    public SingletonDependent(MyLogger myLogger) {
        this.myLogger = myLogger;
        myLogger.log("init SingletonDependent");
    }
}

