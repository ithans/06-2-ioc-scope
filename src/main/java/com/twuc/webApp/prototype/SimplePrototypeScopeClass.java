package com.twuc.webApp.prototype;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class SimplePrototypeScopeClass {
    private MyLogger logger;

    public SimplePrototypeScopeClass(MyLogger logger) {
        this.logger = logger;
        this.logger.log("simplePrototype");
    }

}
