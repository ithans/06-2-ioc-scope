package com.twuc.webApp.prototype;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MyLogger {
    private List<String> lines = new ArrayList<>();

    public void log(String log) {
        lines.add(log);
    }

    public List<String> getLines() {
        return lines;
    }

}
