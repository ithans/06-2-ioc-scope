package com.twuc.webApp.singleton;

import com.twuc.webApp.singleton.Extend;
import com.twuc.webApp.singleton.InterfaceOne;
import org.springframework.stereotype.Component;

@Component
public class InterfaceOneImpl extends Extend implements InterfaceOne {
}
